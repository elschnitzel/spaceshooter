﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Limits
{
    public float yMin, yMax, xMin, xMax;
}

public class PlayerController : MonoBehaviour {

    public Limits limits;
    public float speed;

    private Rigidbody player;


    private void Start()
    {
        player = GetComponent<Rigidbody>();
        if (player == null)
        {
            throw new System.Exception("No Rigidbody found!");
        }
    }

    void FixedUpdate() {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(horizontal, vertical, 0);
        player.velocity = movement * speed;

        player.position = new Vector3(
            Mathf.Clamp(player.position.x, limits.xMin, limits.xMax),
            Mathf.Clamp(player.position.y, limits.yMin, limits.yMax),
            0f);
        player.rotation = Quaternion.Euler(player.transform.rotation.x + 15,player.rotation.y,player.rotation.z);
       
    }
}
